import styles from "../assets/css/todo.module.css";
import { useContext, useEffect, useState } from "react";
import MyContext from "../context/context";

const TodoItem = ({text, id}) => {
    
    const context = useContext(MyContext);

    const [isEdit, changeMode] = useState(false);

    const [updatedText, updateText] = useState(false);


    useEffect(()=>{
        updateText(text)
    }, [])

    const editHandler = () => {
        if(!updatedText) return;
        changeMode(false);
        context.editItem(id, updatedText)
    }

    const changeHandler = (ev) => {
        updateText(ev.target.value)
    }

    let content = <div onClick={()=>changeMode(true)}>{text}</div>;
    if(isEdit)
        content = <input value={updatedText} onBlur={editHandler} onChange={changeHandler}/>
    return content;
};

export default TodoItem;
