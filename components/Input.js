import styles from "../assets/css/input.module.css";
import { useContext, useState } from "react";
import MyContext from "../context/context";

const Input = props => {
  
  const context = useContext(MyContext);
  const [text, setText]= useState('')

  const add = () =>{
    if(!text) return;
    context.addItem(text);
    setText('')
  }

  return (
    <div className={styles.inputContainer}>
        <input placeholder="Want to add a task?" onChange={(e)=>setText(e.target.value)} value={text} className={styles.input} type="text" />
        <button className={styles.add} onClick={add}>
          Add Task
        </button>
    </div>
  );
};

export default Input;
