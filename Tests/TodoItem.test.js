import { mount } from "enzyme";
import TodoItem from "../components/TodoItem.js";

describe("Todo Item Component", () => {
  let wrapper;
  beforeEach(() => {
    let text = "Task Text";
    wrapper = mount(<TodoItem text={text} />);
  });

  it("todo Item  renders without crashing", () => {
    expect(wrapper).not.toBeNull();
  });

  it("todo item should show props text correctly", () => {
    expect(wrapper.find("div").text()).toEqual("Task Text");
  });

  it("todo item should switch to edit when click div", () => {
    wrapper.find("div").simulate("click");
    expect(wrapper.find("input")).not.toBeNull();
  });

  it("updates the input value", ()=>{
    wrapper.find("div").simulate("click");
    const input = wrapper.find("input");
    
    input.simulate("change", {
      target: {
        value: "Updated Text",
      },
    });

    expect(input.instance().value).toEqual("Updated Text");

  })


  it("update the text correctly", () => {
    wrapper.find("div").simulate("click");
    const input = wrapper.find("input");
    input.simulate("focus");
    input.simulate("change", {
      target: {
        value: "Updated Text",
      },
    });
    input.simulate("blur");
    wrapper.setProps({ text: "Updated Text" });
    expect(wrapper.find("div").text()).toEqual("Updated Text");
  });
});
