import AboutLayout from "../components/AboutLayout";

const About = () => (
    <AboutLayout>
        <h1>Task Management Board Application</h1>
    </AboutLayout>
);

export default About;
