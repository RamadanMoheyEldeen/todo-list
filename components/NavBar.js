import Link from "next/link";
import Input from '../components/Input'

const NavBar = () => (
    <nav className="navbar navbar-expand">
      <Input />
      <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item m-3">
            <Link href="/"><a className="nav-link text-dark">Home</a></Link>
          </li>
          <li className="nav-item m-3">
            <Link href="/about"><a className="nav-link text-dark">About</a></Link>
          </li>
        </ul>
    </div>
    <style jsx>
      {
        `
        .navbar{
          background-color: rgba(247,247,247);
          border-bottom: 1px solid rgba(230, 230, 230);
          padding: 0 50px
        }
        `
      }
    </style>
  </nav>
  
);

export default NavBar;
