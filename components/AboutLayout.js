import styles from '../assets/css/layout.module.css'
import NavBar from "./NavBar";
import Head from "next/head";

const Layout = (props) => (
  <div>
    <Head>
      <title> About</title>
      <link
        rel="stylesheet"
        href="https://bootswatch.com/4/cerulean/bootstrap.min.css"
      />
    </Head>
    <NavBar />
    <div className={styles.about}>
       {props.children}
    </div>
  </div>
);

export default Layout;
