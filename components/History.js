import styles from "../assets/css/input.module.css";
import { useContext } from "react";
import MyContext from "../context/context";

const History = () => {
  const context = useContext(MyContext);    
   if(context.activeItem && context.activeItem.history && context.activeItem.history.length)
    return (
        <div data-cy='historyContainer' onClick={() => context.setActive({})} className={styles.historyContainer}>
        <div data-cy='history' onClick={(e) => e.stopPropagation() } className={styles.popUp}>
          <table >
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Action</th>
                <th scope="col">Status</th>
                <th scope="col">Time</th>
              </tr>
            </thead>
            <tbody>
              {context.activeItem.history.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <th scope="row">{index + 1}</th>
                    <td>{item.action}</td>
                    <td>{item.status}</td>
                    <td>{item.time.toUTCString()}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
 else {
     return <></>
 }
};

export default History;
