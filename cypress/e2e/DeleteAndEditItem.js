describe("Add item", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
    cy.get("input").type("First Task");
    cy.get("button").click();
  });

  it("Delete Item", () => {
    cy.get("[data-cy=todoitem] a").last().click();
    cy.get("[data-cy=todoitem]").should("have.length", 0);
  });

  it("Edit Item", () => {
    cy.get("[data-cy=todoitem] div").last().click();
    cy.get('[data-cy=todoitem] input').clear().type("This is updated task").blur();
    cy.get('#todo').should("contain", 'This is updated task')

  });


});
