
describe("Moving list item", () => {
    beforeEach(()=>{
      cy.visit("localhost:3000");
      cy.get("input").type("New Task");
      cy.get("button").click();
    })
  
    it("Open History", () => {
        cy.get('[data-cy=todoitem] a').first().click()
        cy.get('[data-cy=history]').should('have.length', 1);
    });

    it("close History when click its container", () => {
        cy.get('[data-cy=todoitem] a').first().click()
        cy.get('[data-cy=historyContainer]').click(10,10)
        cy.get('[data-cy=historyContainer]').should('have.length', 0);
    });

    it("Does not close History when click popup", () => {
        cy.get('[data-cy=todoitem] a').first().click()
        cy.get('[data-cy=history]').click();
        cy.get('[data-cy=historyContainer]').should('have.length', 1);
    });

    it("History keep track of all movements ", () => {
        cy.get('[data-cy=todoitem]').drag('#inProgress', {force:true});
        cy.get('[data-cy=inProgressitem]').drag('#todo', {force:true});
        cy.get('[data-cy=todoitem] a').first().click()
        cy.get('[data-cy=historyContainer] tr').should('have.length', 4);
    });


  });
  