describe('Add item', () => {

    beforeEach(() => {
       return cy.visit('http://localhost:3000')    
  })


  it('Does not add empty items', () => {
    cy.get('button').click();
    cy.get('button').click();
    cy.get('[data-cy=todoitem]').should('have.length', 0)
  })

  it('Add New Item', () => {
    cy.get('input').type("First Task");
    cy.get('button').click();
    cy.contains('First Task').should('be.visible') 
  })

  it('Add Many Items', () => {
    cy.get('input').type("First Task");
    cy.get('button').click();
    cy.get('input').type("Second Task");
    cy.get('button').click();
    cy.get('[data-cy=todoitem]').should('have.length', 2)
  })

  it('Does not add a task with the same name', () => {
    cy.get('input').type("First Task");
    cy.get('button').click();
    cy.get('input').type("First Task");
    cy.get('button').click();
    cy.get('[data-cy=todoitem]').should('have.length', 1);
  })

})
