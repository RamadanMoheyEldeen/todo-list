export const SET_ITEMS = "SET_ITEMS";
export const ADD_ITEM = "ADD_ITEM";
export const CHANGE_STATUS = "CHANGE_STATUS";
export const DELETE_ITEM = "DELETE_ITEM";
export const EDIT_ITEM = "EDIT_ITEM";
export const SET_ACTIVE = "SET_ACTIVE";


const setItems = (items, state) => {
  return { ...state, items };
};

const setActive = (activeItem, state) => {
  return {...state, activeItem}
}

const addItem = (text, state) => {
  let isExist = state.items.some(i => i.text ==text );
  if(isExist) return { ...state }
  let item = {
    id: Date.now(),
    text,
    status: "todo",
    history: [
      {
        time: new Date(),
        action: "Create Item",
        status: "todo",
      },
    ],
  };
  return { ...state, items: [...state.items, item] };
};

const changeStatus = (id, status, state) => {
  let historyItem = null;
  let items = state.items.map((item) => {
    if (item.id == id) {
      historyItem = {
        time: new Date(),
        action: `Change Status`,
        status,
      };
      return Object.assign({}, item, {
        status,
        history: [...item.history, historyItem],
      });
    }
    return item;
  });

  return { ...state, items };
};

const deleteItem = (id, state) => {
  return { ...state, items: state.items.filter((i) => i.id != id) };
};

const editItem = (id, text, state) => {
  let item = state.items.find((i) => i.id == id);
  if (item.text == text) return { ...state };
  let historyItem = null;
  let items = state.items.map((item) => {
    if (item.id == id) {
      historyItem = {
        time: new Date(),
        action: `Change Text`,
        status: item.status,
      };
      return Object.assign({}, item, {
        text,
        history: [...item.history, historyItem],
      });
    }
    return item;
  });
  return { ...state, items };
};

export const Reducer = (state, action) => {
  switch (action.type) {
    case SET_ITEMS:
      return setItems(action.items, state);
    case SET_ACTIVE: 
      return setActive(action.activeItem, state);
    case ADD_ITEM:
      return addItem(action.text, state);
    case CHANGE_STATUS:
      return changeStatus(action.id, action.status, state);
    case DELETE_ITEM:
      return deleteItem(action.id, state);
    case EDIT_ITEM:
      return editItem(action.id, action.text, state);
    default:
      return state;
  }
};
