
describe("Moving list item", () => {
  beforeEach(()=>{
    cy.visit("localhost:3000");
    cy.get("input").type("First Task");
    cy.get("button").click();
  })

  it("Change new item to in progress status", () => {
    cy.get('[data-cy=todoitem]').drag('#inProgress', {force:true});
    cy.get('#inProgress').should("contain", 'First Task');
  });

  it("Change new item to done status", () => {
    cy.get('[data-cy=todoitem]').drag('#done', {force:true});
    cy.get('#done').should("contain", 'First Task');
  });

  it("Change status to todo again", () => {
    cy.get('[data-cy=todoitem]').drag('#inProgress', {force:true});
    cy.get('[data-cy=inProgressitem]').drag('#todo', {force:true});
  });

  it("Does not drag item from done", () => {
    cy.get('[data-cy=todoitem]').drag('#done', {force:true});
    cy.get('[data-cy=doneitem]').drag('#todo');
    cy.get('#done').should("contain", 'First Task');
  });

});
