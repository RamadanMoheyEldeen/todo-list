import React from 'react';

export default React.createContext({
  items: [],
  activeItem: {},
  setActive: (item)=>{},
  setItems: (items)=>{},
  addItem: (text)=>{},
  changeStatus: (id, status)=> {},
  deleteItem: (id)=>{},
  editItem: (id, text)=> {},
});
