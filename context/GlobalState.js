import React, { useReducer, useEffect } from "react";

import MyAppContext from "./context";
import { Reducer, SET_ITEMS, SET_ACTIVE ,ADD_ITEM, CHANGE_STATUS, DELETE_ITEM, EDIT_ITEM } from "./reducers";

const GlobalState = (props) => {

  const [myState, dispatch] = useReducer(Reducer, {
    items: []
  });

  const setItems = (items) => {
    dispatch({ type: SET_ITEMS, items });
  };

  const setActive = (activeItem) => {
    dispatch({ type: SET_ACTIVE, activeItem });
  };

  const addItem = (text) => {
    dispatch({ type: ADD_ITEM, text });
  };

  const changeStatus = (id, status) => {
    dispatch({ type: CHANGE_STATUS, id , status});
  };

  const deleteItem = (id) =>{
    dispatch({type: DELETE_ITEM, id})
  }

  const editItem = (id, text) => {
    dispatch({ type: EDIT_ITEM, id , text});
  };

  useEffect(() => {
    localStorage.setItem('todoItems', JSON.stringify(myState.items))
    console.log(myState.items)
  }, [myState.items])

  return (
    <MyAppContext.Provider
      value={{
        items: myState.items,
        activeItem: myState.activeItem,
        setActive: setActive,
        setItems: setItems,
        addItem: addItem,
        changeStatus: changeStatus,
        deleteItem: deleteItem,
        editItem: editItem
      }}
    >
      {props.children}
    </MyAppContext.Provider>
  );
};

export default GlobalState;
