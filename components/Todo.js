import styles from "../assets/css/todo.module.css";
import { useContext, useEffect } from "react";
import MyContext from "../context/context";
import TodoItem from "./TodoItem";

const Todo = (props) => {
  const context = useContext(MyContext);  
  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("todoItems"));
    context.setItems(items || []);
  }, []);

  const drag = (ev) => {
    ev.dataTransfer.setData("id", ev.target.id);
  };

  const allowDrop = (ev) => {
    ev.preventDefault();
  };

  const drop = (ev) => {
    ev.preventDefault();
    let id = ev.dataTransfer.getData("id");
    context.changeStatus(id, props.status);
  };

  const deleteItem = (id) => {
    context.deleteItem(id);
  };

  const showHistory = (item) => {
      context.setActive(item)
  }


  let isDraggable = false;
  if (props.status == "inProgress" || props.status == "todo")
    isDraggable = true;

  return (
    <div
      onDrop={() => drop(event)}
      onDragOver={() => allowDrop(event)}
      className={`${styles.todo} ${styles[props.status]}`}
      id={props.status}
    >
      {context.items
        .filter((i) => i.status == props.status)
        .map((item) => (
          <div
            data-cy={props.status + 'item'}
            draggable={isDraggable}
            onDragStart={drag}
            id={item.id}
            key={item.id}
            className={styles.wrapper}
          >
            <div className={styles.actions}>  
              <a onClick={() => showHistory(item)}> History </a>
              <a onClick={() => deleteItem(item.id)}> Delete </a>
            </div>
            <TodoItem {...item} />
          </div>
        ))}
    </div>
  );
};

export default Todo;
