module.exports = {
    collectCoverageFrom: [
      '**components/*.{js,jsx}',
    ],
    coverageThreshold: {
      global: {
        branches: 100,
        functions: 100,
        lines: 100,
        statements: 100,
      },
    },
    moduleNameMapper: {
      '\\.module\\.css$': 'identity-obj-proxy'
    },
    setupFiles: [
      '<rootDir>/test-config/setup.js',
    ],
    setupFilesAfterEnv: [
      '<rootDir>/test-config/setupAfterEnv.js',
    ],
    testMatch: [
      '**/Tests/?(*.)+(spec|test).[jt]s?(x)',
    ],
    testPathIgnorePatterns: [
      '/.next/',
      '/node_modules/',
      '/tests/',
      '/coverage/'
    ],
    transform: {
      '^.+\\.jsx?$': 'babel-jest',
    },
  };