import styles from "../assets/css/layout.module.css";
import NavBar from "./NavBar";
import Head from "next/head";

const Layout = (props) => (
  <div>
    <Head>
      <title> Management Dashboard </title>
      <link
        rel="stylesheet"
        href="https://bootswatch.com/4/cerulean/bootstrap.min.css"
      />
    </Head>
    <NavBar />
    <div className={styles.app}>
      <div className={styles.wrapper}>{props.children}</div>
    </div>
  </div>
);

export default Layout;
