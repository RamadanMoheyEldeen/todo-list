import Layout from "../components/Layout";
import GlobalState from "../context/GlobalState";
import Todo from "../components/Todo";
import History from "../components/History";

const Index = () => {
 return ( 
  <GlobalState>
    <Layout>
        <History/>
        <Todo status='todo'/>
        <Todo status='inProgress'/>
        <Todo status='done'/>
    </Layout>
  </GlobalState>
)}

export default Index;
