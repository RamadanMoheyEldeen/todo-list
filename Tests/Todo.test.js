import { mount, shallow } from "enzyme";
import Todo from "../components/Todo.js";

describe("Todo Component", () => {
  let wrapper;
  let items =  [
    {
        text: 'First task',
        id: '14484444',
        status: 'todo'
    },
    {
        text: 'Second task',
        id: '55411112',
        status: 'todo'
    }
    ]

  beforeEach(() => {
    wrapper = mount(<Todo status='todo' />, {context: {items}} );
  });

  it("Renders Todo", () => {
    expect(wrapper).not.toBeNull();
  });
  
});
