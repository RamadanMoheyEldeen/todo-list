import { mount, shallow } from "enzyme";
import History from "../components/History.js";

describe("Todo History", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<History />);
  });

  it("Renders History", () => {
    expect(wrapper).not.toBeNull();
  });
  
});
