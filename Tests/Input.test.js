import { mount } from "enzyme";
import Input from "../components/Input.js";

describe("Input Component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(<Input />);
  });

  it("to renders Input Component without crashing", () => {
    expect(wrapper).not.toBeNull();
  });

  it("Test changing input text", () => {
    const inputField = wrapper.find("input");
    inputField.simulate("change", {
      target: {
        value: "A new task",
      },
    });
    expect(wrapper.find("input").instance().value).toEqual("A new task");
  });


  it("Test add button", () => {
    wrapper.find("input").simulate("change", {
      target: {
        value: "A new task",
      },
    });
    wrapper.find("button").simulate('click');
    expect(wrapper.find("input").instance().value).toEqual('');
  });
});
